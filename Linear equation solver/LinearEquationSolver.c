/************************
*
* A linear Equation Solver
*
*
* Author: Maruf
* Email: mahm1846@uni.sydney.edu.au
**************************/

#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include "mpi.h"

#define  MXDIM  12
#define  CLSIZE 4
#define  N     1000
#define  ERROR 1.0e-2
//#define  ERROR 1.0e-6

int main (int argc, char ** argv )
{
    int rank, mpi_size;
    int tag_up, tag_down;
    MPI_Status status;
    double T_start, T_end, T_total;

    double local_x   [MXDIM/CLSIZE + 2 ][MXDIM];
    double updated_x [MXDIM/3 + 2 ][MXDIM];
    double input_x   [MXDIM][MXDIM];
    int  first, last;
    int k;
    double global_err_sqrt, local_err_sq_sum;

    FILE *input_fp;

    MPI_Init (&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    MPI_Comm_size (MPI_COMM_WORLD, &mpi_size);

    if (mpi_size != CLSIZE)
    {
        if (rank == 0)
            printf("Please specify exactly 4 processors.\n") ;
        MPI_Abort(MPI_COMM_WORLD, 1);
    }


    if (rank == 0)
    {
         input_fp = fopen( "data.txt", "r");
         if (input_fp == NULL)
         {
             printf( "Cannot input read file. ");
             MPI_Abort(MPI_COMM_WORLD, 1);
         }

         for (int i = MXDIM -1; i >= 0; i --)
         {
             for (int j = 0; j < MXDIM; j ++)
                  fscanf(input_fp, "%lf", &input_x[i][j]);
             fscanf(input_fp, "\n");
         }

         printf("Input \n");
         for (int i = MXDIM -1; i >= 0; i --)
         {
             for (int j = 0; j < MXDIM; j ++)
                  printf( "%8.5f", input_x[i][j]);
              printf( "\n");
        }
    }

    T_start = MPI_Wtime();

    MPI_Scatter( input_x[0], MXDIM * (MXDIM / mpi_size), MPI_DOUBLE,
                 local_x[1], MXDIM * (MXDIM /mpi_size), MPI_DOUBLE,
                 0, MPI_COMM_WORLD );


    first = 1;
    last  = MXDIM/CLSIZE;
    /* The top and bottom processors, each have one less interior row each.
    */
    if (rank == 0)
        first++;
    if (rank == CLSIZE -1)
        last --;


    /* Fill the arrays with data */
    /*
    for (int i = 1; i <= MXDIM/CLSIZE; i++)
        for (int j = 0; j < MXDIM; j++)
            local_x[i][j] = rank;
    */
    /* Fill the top and bottom row with boundary values */
    /*
    for (int j = 0; j < MXDIM; j++)
    {
         local_x [first - 1][j] = -1;
         local_x [last  + 1][j] = -1;
    }
    */
    global_err_sqrt = 1000;

    tag_down = 1;
    tag_up = 2;
    k = 0;


    while (global_err_sqrt > ERROR && k < N)
    {
          /* Sending data to a higher ranked process.
             Sending the bottom row of the local matrix to the upper ranked process.
             Lower ranked processor is receiving in the top matrix row */

           if (rank < mpi_size - 1)
                MPI_Send( local_x [MXDIM/mpi_size], MXDIM, MPI_DOUBLE,
                         rank + 1, tag_up, MPI_COMM_WORLD );

           if (rank > 0)
                MPI_Recv(local_x [0], MXDIM, MPI_DOUBLE,
                         rank - 1, tag_up, MPI_COMM_WORLD, &status );

           /* Send data to the next lower ranked processor.
              Sending the first row of local matrix to the next lower ranked processor
              The lower ranked processor receives in the last row of the matrix. */

           if (rank > 0)
                MPI_Send(local_x[1], MXDIM,MPI_DOUBLE,
                         rank - 1, tag_down, MPI_COMM_WORLD);

           if (rank < mpi_size -1)
                MPI_Recv(local_x[MXDIM/mpi_size+1], MXDIM, MPI_DOUBLE,
                         rank + 1, tag_down, MPI_COMM_WORLD, &status);


            local_err_sq_sum = 0.0;
            for (int i = first; i <= last; i ++)
                for (int j = 1; j < MXDIM - 1; j ++ )
                {
                   updated_x[i][j]= (local_x[i][j+1] + local_x[i][j-1]+
                                     local_x[i+1][j] + local_x[i-1][j])/4.0;
                   local_err_sq_sum += (updated_x[i][j] - local_x[i][j]) *
                                    (updated_x[i][j] - local_x[i][j]);
                }

           for (int i = first; i <= last; i ++)
                for (int j = 1; j < MXDIM - 1; j ++ )
                {
                   local_x[i][j]= updated_x [i][j] ;
                }

           MPI_Allreduce(&local_err_sq_sum, &global_err_sqrt, 1, MPI_DOUBLE, MPI_SUM,
                         MPI_COMM_WORLD);
           global_err_sqrt = sqrt (global_err_sqrt);


           if (rank == 0)
                 printf("Itr. No %4d, Total Err. %e\n",
                        k, global_err_sqrt);
           k ++;
    }

    MPI_Gather(  local_x[1], MXDIM * (MXDIM / mpi_size), MPI_DOUBLE,
                 input_x , MXDIM * (MXDIM /mpi_size), MPI_DOUBLE,
                 0, MPI_COMM_WORLD );


    T_end = MPI_Wtime();
    if (rank == 0)
    {
        T_total = T_end - T_start;
        printf( "MPI Solution\n");


        for (int i = MXDIM -1; i >= 0; i --)
        {
             for (int j = 0; j < MXDIM; j ++)
                  printf( "%8.5f", input_x[i][j]);
             printf( "\n");
        }
        printf( "Global Error: %e\n", global_err_sqrt);
        printf( "Total Processors: %d, Wall Time: %10.5f\n", mpi_size, T_total);

    }

     /* Shut down MPI */
     MPI_Finalize();
     return 0;
}
