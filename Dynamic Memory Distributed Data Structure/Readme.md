# Dynamic Parallel 2D array data distribution

This program uses MPI to send and receive the adjacent edges to the  neighboring  processors.

A dynamically allocated MxM array is equally distributed among the processors and boundary rows are exchanged among the adjacent processors. 

The top and bottom rows are exchanged with neighboring processors.  

To compile:
```bash
mpicc - o ParallelDateStructure  ParallelDateStructure.c
```
To run:
```bash
mpiexec -n 4 ParallelDateStructure
```
