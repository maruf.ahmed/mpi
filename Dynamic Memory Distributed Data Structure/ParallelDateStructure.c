/************************
*
* Parallel distributed Data Structure using 2D dynamic memory allocation.
*
*
* Author: Maruf
* Email: mahm1846@uni.sydney.edu.au
**************************/


#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define M 24
#define LIMIT 10

  void MpiErrMsg(int error, int rank, char* string)
  {
        fprintf(stderr, "Process %d: Error %d in %s\n", rank, error, string);
        MPI_Finalize();
        exit(-1);
  }

  void ChkErrMsg(int error, int rank, char* string)
  {
     if (errno != 0)
     {
         fprintf(stderr, "Process %d: Error %d in %s\n", rank, error, string);
         fprintf(stderr, "The error message: %s\n", strerror(error));
     }
  }


  void print_2D (double ** a, int m, int n )
  {
       printf("\n");
       for (int i = 0; i < m; i++)
       {
            for(int j = 0; j < n; j++)
                printf("%6.2f", a[i][j]);
            printf("\n");
        }
  }



  int main (int argc, char **argv)
  {

       int rank, size;
       MPI_Status status;
       int error;

       double **array = NULL;


       MPI_Init (&argc, &argv);
       MPI_Comm_rank(MPI_COMM_WORLD, &rank);
       MPI_Comm_size(MPI_COMM_WORLD, &size);

       if (size < 2)
       {
            fprintf(stderr, "This program needs at least 2 processors to run,\n"
                            "Processors found %d, Aborting ...", size);
            /* MPI_Abort(MPI_COMM_WORLD, 1); */
            MPI_Finalize();
            exit(-1);
       }


       array = malloc(M * sizeof(double*));
       array[0] = malloc(M * M * sizeof(double));
       for (int i = 1; i < M; i++)
       {
           array[i] = array[0] + M * i;
       }


       /* Fill the inner rows with data
         Except the first and last row
       */
       for (int i = 1; i <= M/size; i++)
            for (int j = 0; j < M ; j ++)
                array [i][j]= ( (double) LIMIT/ RAND_MAX) * rand();

       /* Fill the first and the last row only */
       for (int j = 0; j < M; j++ )
       {
            array[0][j] = -1;
            array[M/size + 1][j] = -1;
       }

       /* ChkErrMsg(errno, rank, "After Value Assigned");*/


       if (rank == 0)
       {
             printf("\nMatrices before the edge rows exchanged\n");
             printf("Rank: %d", 0);
             print_2D (array, M/size + 2, M );
             for (int i = 1; i < size; i ++)
             {
                  printf("Rank: %d", i);
                  error = MPI_Recv(&(array[0][0]), (M/size + 2)*M, MPI_DOUBLE, i,0, MPI_COMM_WORLD, &status );
                  if(error != MPI_SUCCESS) MpiErrMsg(error, rank, "MPI_Recv_Error");
                  print_2D (array, M/size + 2, M );
             }

       }
       else
       {
             error = MPI_Send(&(array[0][0]), (M/size + 2)*M, MPI_DOUBLE, 0,0, MPI_COMM_WORLD );
             if(error != MPI_SUCCESS) MpiErrMsg(error, rank, "MPI_Send_Error");
       }
       /* ChkErrMsg(errno, rank, "After Initial values transmitted and printed");*/

        /* Ranks, Sending and receiving data */
        /* -------
              0       x_local [M/size]
           -------
              1       x_local [0]
           -------
              2
           -------
              3
           -------
         */
         /* Send the second bottom row to the next higher process */
       if (rank < size - 1)
              MPI_Send(array[M/size], M, MPI_DOUBLE, rank + 1, 1, MPI_COMM_WORLD);

       if (rank > 0)
             MPI_Recv(array[0], M, MPI_DOUBLE, rank - 1, 1, MPI_COMM_WORLD, &status);

         /* Send the second row to the previous ranked process.
            Because the first row will received values from the other processor.
         */
       if (rank > 0)
              MPI_Send(array[1], M, MPI_DOUBLE, rank -1, 2, MPI_COMM_WORLD  );
       if (rank < size - 1)
              MPI_Recv(array [M/size + 1], M, MPI_DOUBLE, rank + 1, 2, MPI_COMM_WORLD, &status );

       /* ChkErrMsg(errno, rank, "After all the rows are exchanged"); */

       if (rank == 0)
       {
             printf("\nMatrices After the edge rows exchanged\n");
             printf("Rank: %d", 0);
             print_2D (array, M/size + 2, M );
             for (int i = 1; i < size; i ++)
             {
                  printf("Rank: %d", i);
                  error = MPI_Recv(&(array[0][0]), (M/size + 2)*M, MPI_DOUBLE, i,3, MPI_COMM_WORLD, &status );
                  if(error != MPI_SUCCESS) MpiErrMsg(error, rank, "MPI_Recv_Error");
                  print_2D (array, M/size + 2, M );
             }

        }
        else
        {
              error = MPI_Send(&(array[0][0]), (M/size + 2)*M, MPI_DOUBLE, 0,3, MPI_COMM_WORLD );
              if(error != MPI_SUCCESS) MpiErrMsg(error, rank, "MPI_Send_Error");
        }

        /* ChkErrMsg(errno, rank, "After the final values transmitted and printed"); */

        error = MPI_Finalize();
        if(error != MPI_SUCCESS) MpiErrMsg(error, rank, "MPI_Finalized");

        free(array[0]);
        free(array);
}


/*
 Useful Links
 Dynamic Memory allocation
 https://stackoverflow.com/questions/61562845/sharing-a-dynamically-allocated-2d-array-with-mpi
 Print in order
 https://www.generacodice.com/es/articolo/1171676/Ordering-Output-in-MPIa=r
 */
