/************************
*
* Parallel MPI-I/O, copy a file
*
*
* Author: Maruf
* Email: mahm1846@uni.sydney.edu.au
**************************/


#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>


  void MpiErrMsg(int error, int rank, char* string)
  {
        fprintf(stderr, "Process %d: Error %d in %s\n", rank, error, string);
        MPI_Finalize();
        exit(-1);
  }


  int main (int argc, char **argv)
  {

       int rank, size;
       MPI_Status status;
       int error;

       int start, end;
       int length;
       char *local_buffer;
       MPI_File    sourcefile, destfile;
       MPI_Offset  filesize;

       if (argc !=3)
       {
              fprintf(stderr, "Usage: %s SourceFile DestFile\n", argv[0]);
              exit(-1);
       }

       MPI_Init (&argc, &argv);
       MPI_Comm_rank(MPI_COMM_WORLD, &rank);
       MPI_Comm_size(MPI_COMM_WORLD, &size);


       /* Read the source file */

       fprintf(stdout, "Proc.%2d: Opening source file: %s\n", rank, argv[1]);
       error = MPI_File_open(MPI_COMM_WORLD, argv[1],
                             MPI_MODE_RDONLY, MPI_INFO_NULL, &sourcefile);
       if (error != MPI_SUCCESS)
               MpiErrMsg(error, rank, "Error: Opening MPI file\n");


       /* Getting the file size */
       error = MPI_File_get_size(sourcefile, &filesize);
       if (error != MPI_SUCCESS)
               MpiErrMsg(error, rank, "Error: Getting MPI file size\n");


       /* Calculating range for each  processor. */
       length = filesize / size;
       start  = length * rank;
       if (rank == size -1)
              end = filesize;
       else
              end = start + length;

       /* Allocating dynamic memory for each file segment to read */
       local_buffer = (char *)malloc((end - start) * sizeof(char));
       if (local_buffer == NULL)
               MpiErrMsg(-1, rank, "Error: Allocating memory\n");



       fprintf(stdout, "Proc.%2d: Reading Bytes in the region = [ %d, %d ) into memory\n",
                                                               rank, start, end);
       MPI_File_seek (sourcefile, start, MPI_SEEK_SET);
       error = MPI_File_read(sourcefile, local_buffer, end-start,MPI_BYTE, &status);

       if (error != MPI_SUCCESS)
               MpiErrMsg(error, rank, "Error: Reading source file at the offset\n");


       MPI_File_close(&sourcefile) ;


       /* Reading the destination file */
       fprintf(stdout, "Proc.%2d: Opening destination file: %s\n", rank, argv[2]);
       error = MPI_File_open(MPI_COMM_WORLD, argv[2],
                             MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &destfile);
       if (error != MPI_SUCCESS)
               MpiErrMsg(error, rank, "Error: Opening MPI file\n");


       fprintf(stdout, "Proc.%2d: Writing Bytes at the region  = [ %d, %d ) from memory\n",
                                                               rank, start, end);
       error = MPI_File_write_at (destfile, start, local_buffer, end - start,
                                  MPI_BYTE, &status);
       if (error != MPI_SUCCESS)
               MpiErrMsg(error, rank, "Error: Writing MPI file\n");


       MPI_File_close(&destfile) ;


       error = MPI_Finalize();
       if(error != MPI_SUCCESS) MpiErrMsg(error, rank, "MPI_Finalized_error\n");

       free(local_buffer);

  }
