# Parallel file copy using MPI-I0 



This program uses MPI-IO to copy a file.

Download both the C file and text file in the same folder.

To compile

```bash
mpicc -o Parallel_IO Parallel_IO.c
```

To run 

```bash
mpiexec -n 4 Parallel_IO.exe  iso_8859-1.txt Dest.txt
```



