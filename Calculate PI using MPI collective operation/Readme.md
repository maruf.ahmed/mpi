# PI value calculation in MPI

This program calculates the value of Pi in parallel using MPI

To compile: 

mpicc -o MPI_Pi MPI_Pi.c

To Run:

mpiexec -n 4 MPI_Pi
