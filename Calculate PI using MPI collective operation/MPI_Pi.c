/************************
* MPI implementation
* Parallel Pi value calculation
*
*
* Author: Maruf
**************************/

#include <stdio.h>
#include <mpi.h>

#define PI25 3.141592653589793238462643

double y (double x)
{
    double result = 4.0 / (1.0 + x*x);
    return result;
}


int main (int argc, char ** argv )
{
    int mpi_size, rank;
    /* short stop_flag; */
    int  INTERVAL = (1024*1024);
    double h;
    double x;
    double local_pi, global_pi;

    double T_start, T_end, T_total;

    int p_namelen;
    char p_name [MPI_MAX_PROCESSOR_NAME];

    MPI_Init (&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    MPI_Comm_size (MPI_COMM_WORLD, &mpi_size);
    MPI_Get_processor_name(p_name, &p_namelen );

    /* Set and broadcast interval */
    if (rank == 0)
    {
        T_start = MPI_Wtime();
        MPI_Bcast(&INTERVAL, 1, MPI_INT, 0, MPI_COMM_WORLD);
    }

    local_pi = 0.0;
    h = 1.0 / (double) INTERVAL;

    for (int i=rank + 1; i <= INTERVAL; i += mpi_size)
    {
        x =  h * ((double)i - 0.5);
        local_pi += y(x) * h;
    }

    printf("Local Process: %d of %d running on the machine %s, Pi value: %f\n", rank, mpi_size, p_name, local_pi);

    MPI_Reduce(&local_pi, &global_pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD );

    if (rank == 0)
    {
        printf("MPI approximate of PI\n");
        printf("Pi approximation: %1.15f\n", global_pi);
        printf("Error: %1.15f\n", (PI25 - global_pi));
        T_end = MPI_Wtime();
        T_total = (T_end - T_start);
        printf("No of processors: %2d, Wall time: %f", mpi_size, T_total);
    }

     MPI_Finalize();
}
