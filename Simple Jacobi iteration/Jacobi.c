/************************
*
* Use Jacobi method to solve linear system of equation
*
*
* Author: Maruf
* Email: mahm1846@uni.sydney.edu.au
**************************/

/************************
Sample system of equations

-1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1
 3  3  3  3  3  3  3  3  3  3  3  3
 3  3  3  3  3  3  3  3  3  3  3  3
 2  2  2  2  2  2  2  2  2  2  2  2
 2  2  2  2  2  2  2  2  2  2  2  2
 2  2  2  2  2  2  2  2  2  2  2  2
 1  1  1  1  1  1  1  1  1  1  1  1
 1  1  1  1  1  1  1  1  1  1  1  1
 1  1  1  1  1  1  1  1  1  1  1  1
 0  0  0  0  0  0  0  0  0  0  0  0
 0  0  0  0  0  0  0  0  0  0  0  0
-1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1

*************************/

#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include "mpi.h"

#define MSIZE  12
#define PROCNO 4
#define  N     1000
//#define  ERROR 1.0e-2
#define  ERROR 1.0e-6

int main (int argc, char ** argv )
{
    int rank, mpi_size;
    int tag_up, tag_down;
    MPI_Status status;
    double T_start, T_end, T_total;

    double local_x   [MSIZE/PROCNO + 2 ][MSIZE];
    double updated_x [MSIZE/3 + 2 ][MSIZE];
    int  first, last;
    int k;
    double global_err_sqrt, local_err_sq_sum;

    MPI_Init (&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    MPI_Comm_size (MPI_COMM_WORLD, &mpi_size);

    if (mpi_size != PROCNO)
    {
        if (rank == 0)
            printf("Please specify exactly 4 processors.\n") ;
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    first = 1;
    last  = MSIZE/PROCNO;

    if (rank == 0)
        first++;
    if (rank == PROCNO -1)
        last --;

    /* Fill the arrays with data */
    for (int i = 1; i <= MSIZE/PROCNO; i++)
        for (int j = 0; j < MSIZE; j++)
            local_x[i][j] = rank;

    /* Fill the top and bottom row with boundary values */
    for (int j = 0; j < MSIZE; j++)
    {
         local_x [first - 1][j] = -1;
         local_x [last  + 1][j] = -1;
    }

    global_err_sqrt = 1000;

    tag_down = 0;
    tag_up = 1;
    k = 0;

    T_start = MPI_Wtime();
    while (global_err_sqrt > ERROR && k < N)
    {
          /* Sending data to a higher ranked process.
             Sending the bottom row of the local matrix to the upper ranked process.
             Lower ranked processor is receiving in the top matrix row */

           if (rank < mpi_size - 1)
                MPI_Send( local_x [MSIZE/PROCNO], MSIZE, MPI_DOUBLE,
                         rank + 1, tag_up, MPI_COMM_WORLD );

           if (rank > 0)
                MPI_Recv(local_x [0], MSIZE, MPI_DOUBLE,
                         rank - 1, tag_up, MPI_COMM_WORLD, &status );

           /* Send data to the next lower ranked processor.
              Sending the first row of local matrix to the next lower ranked processor
              The lower ranked processor receives in the last row of the matrix. */

           if (rank > 0)
                MPI_Send(local_x[1], MSIZE,MPI_DOUBLE,
                         rank - 1, tag_down, MPI_COMM_WORLD);

           if (rank < mpi_size -1)
                MPI_Recv(local_x[MSIZE/mpi_size+1], MSIZE, MPI_DOUBLE,
                         rank + 1, tag_down, MPI_COMM_WORLD, &status);


            local_err_sq_sum = 0.0;
            for (int i = first; i <= last; i ++)
                for (int j = 1; j < MSIZE - 1; j ++ )
                {
                   updated_x[i][j]= (local_x[i][j+1] + local_x[i][j-1]+
                                     local_x[i+1][j] + local_x[i-1][j])/4.0;
                   local_err_sq_sum += (updated_x[i][j] - local_x[i][j]) *
                                    (updated_x[i][j] - local_x[i][j]);
                }

           for (int i = first; i <= last; i ++)
                for (int j = 1; j < MSIZE - 1; j ++ )
                {
                   local_x[i][j]= updated_x [i][j] ;
                }

           MPI_Allreduce(&local_err_sq_sum, &global_err_sqrt, 1, MPI_DOUBLE, MPI_SUM,
                         MPI_COMM_WORLD);
           global_err_sqrt = sqrt (global_err_sqrt);


           if (rank == 0)
                 printf("Itr. No %4d, Total Err. %e\n",
                        k, global_err_sqrt);
           k ++;
    }

    T_end = MPI_Wtime();
    if (rank == 0)
    {
        T_total = T_end - T_start;
        printf( "MPI results\n");
        printf( "Global Error: %e\n", global_err_sqrt);
        printf( "Total Processors: %d, Wall Time: %10.5f", mpi_size, T_total);
    }

     /* Shut down MPI */
     MPI_Finalize();
}
