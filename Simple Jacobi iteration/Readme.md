# A simple Jacobi Iteration method

Jacobi method to solve a system of linear equations

To compile:
mpicc -o jacobi jacobi.c -lm 

To run:
mpiexec -np 4 jacobi
