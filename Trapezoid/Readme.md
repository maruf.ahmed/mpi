# Trapezoid formula in MPI

This MPI program uses trapezoid rule to calculate the integral of a given function. 

To compile: 
mpicc -o Trapezoid Trapezoid.c

To Run:
mpiexec -n 4 Trapezoid
