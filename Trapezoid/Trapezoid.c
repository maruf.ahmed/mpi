/************************
*
* Parallel Trapezoid Integral Calculation in MPI
*
*
* Author: Maruf
**************************/

#include <stdio.h>
#include <mpi.h>


/* Calculates the function y of x */
double y (double x)
{
    return 5*x*x*x*x;
}

/* Calculate Trapezoid rule */

double trapezoid (double a, double b, long n, double h)
{
    double result = 0;
    double x = 0;

    result  =  (y(a) + y(b)) * h / 2.0;
    x = a;
    for (long i = 1; i < n; i++)
    {
         x += h;
         result += y(x) * h;
    }
    return result;
}

int main (int argc, char ** argv )
{
    int mpi_size;
    int rank;
    double a, b, h;
    long n;
    double local_a, local_b, local_n, local_result;
    double result, global_result, error, actual_result;
    int source, dest, tag;
    MPI_Status status;
    double T_start, T_end, T_total;

    a = 0.0;  /* Left end point */
    b = 1.0;  /* Right end point */
    n = 1024*1024;  /* Number of divisions */

    MPI_Init (&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    MPI_Comm_size (MPI_COMM_WORLD, &mpi_size);

    T_start = MPI_Wtime();

    h = (b-a)/n;  /* Width of each Trapezoid section */

    /* Calculate Trapezoids Local to each processor */
    local_n = n/mpi_size;
    local_a = a + rank * local_n * h;
    local_b = local_a + local_n * h;

    /* Call the trapezoid function with local values for the processor */
    local_result  = trapezoid(local_a, local_b, local_n, h);

    /* printf("Local Rank: %d, Result: %lf\n", rank, local_result); */


    tag = 0;
    dest = 0; /* All data is send to the first processor */
    /* Collect all the sub result */
    if (rank == 0 )
    {
        global_result = local_result;
        for (source = 1; source < mpi_size; source++)
        {
            MPI_Recv(&local_result, 1, MPI_DOUBLE, source, tag,
                     MPI_COMM_WORLD, &status);
            global_result += local_result;
        }
    }
    /* Otherwise send the results */
    else
    {
        MPI_Send(&local_result, 1, MPI_DOUBLE, dest, tag, MPI_COMM_WORLD );
    }

    T_end = MPI_Wtime();

   /* Print Results in the first process */
   actual_result = 1.0e0;

   if (rank == 0 )
   {
       error = global_result - actual_result;
       T_total = T_end - T_start;

       printf( "MPI results\n");
       printf( "Total Trapezoids: %4ld, Integral limits (%6.2lf, %6.2lf), Result: %10.10lf, Error: %10.10lf \n",
                            n, a, b, global_result, error);
       printf( "Total Processors: %d, Wall Time: %10.5lf", mpi_size, T_total);
   }

     /* Shut down MPI */
     MPI_Finalize();
}
