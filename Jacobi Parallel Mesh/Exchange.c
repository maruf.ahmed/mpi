#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include "jacobi.h"
#include "mesh.h"

void Exchange (Mesh *mesh)
{
    MPI_Status status;
    double  *xlocal    = mesh ->xlocal;
    int      maxm      = mesh ->maxm;
    int      lrow      = mesh ->lrow;
    int      up_nbr    = mesh ->up_nbr;
    int      down_nbr  = mesh ->down_nbr;
    MPI_Comm ring_comm = mesh ->ring_comm;

    /* Send to the up process and receive from below process */
    MPI_Send(xlocal + maxm * lrow, maxm, MPI_DOUBLE,
             up_nbr, 0, ring_comm);
    MPI_Recv(xlocal, maxm, MPI_DOUBLE,
             down_nbr, 0, ring_comm, &status);

    /* Send to the down process, and receive from the above */
    MPI_Send(xlocal + maxm, maxm, MPI_DOUBLE,
             down_nbr, 1, ring_comm);
    MPI_Recv(xlocal + maxm * (lrow + 1), maxm, MPI_DOUBLE,
             up_nbr, 1, ring_comm, &status);

}
