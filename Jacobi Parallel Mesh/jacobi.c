#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include "mpi.h"
#include "jacobi.h"
#include "mesh.h"
#include "cmdline.h"
#include "setupmesh.h"
#include "Exchange.h"

/*
 * do_print: a variable that controls the debugging output
 */
 static int do_print = 0;
 static int max_it = 1000; /*100;*/

 /*
  * NO_CONV_TEST flag to differentiate among the schemes
  */

  int main (int argc, char ** argv)
  {
      int rank, size, itcnt;
      Mesh mesh;

      #ifndef NO_CONV_TEST
          double diffnorm;
          double gdiffnorm;
      #else
          #define gdiffnorm 1.0
      #endif // NO_CONV_TEST

      double *swap;
      double *xlocalrow, *xnewrow;

      int maxm, maxn, lrow;
      //int     k;
      double  t;

      MPI_Init (&argc, &argv);
      MPI_Comm_rank (MPI_COMM_WORLD, &rank);
      MPI_Comm_size (MPI_COMM_WORLD, &size);

      Get_command_line(rank, argc, argv, &maxm, &maxn, &do_print, &max_it);

      Setup_mesh(maxm, maxn, &mesh);

      /* Run the entire setup twice to make sure that the code cached
       * Take the second runtime  wall time
       */

       for (int k = 0; k < 2; k ++)
       {
           itcnt = 0;
           Init_mesh (&mesh);
           MPI_Barrier (mesh.ring_comm);

           t = MPI_Wtime();

           if (do_print==1 && rank == 0)
           {
               printf("Start time: %f\n", t);
               fflush(stdout);
           }

           /* lrow is used to make a local copy */
           lrow = mesh.lrow;

           do
           {
               Exchange( &mesh);
               /* Compute new set of values */
               itcnt ++;

               #ifndef NO_CONV_TEST
               diffnorm = 0.0;
               #endif // NO_CONV_TEST

               xnewrow   =  mesh.xnew + 1 * maxm ;
               xlocalrow =  mesh.xlocal + 1 * maxm;

               for(int i = 1; i <= lrow; i ++)
               {
                   for (int j = 1; j < maxm - 1; j++)
                   {
                        double diff;
                        xnewrow [j] = (double)( xlocalrow[j + 1] +
                                        xlocalrow[j - 1] +
                                        xlocalrow[maxm + j ] +
                                        xlocalrow[-maxm + j]
                                        ) / 4.0;
                        /* Accumulating diffnorm
                           may help to reduce cache misses for large data
                        */
                        #ifndef NO_CONV_TEST
                        diff = xnewrow[j] - xlocalrow[j];
                        diffnorm += diff * diff;
                        #endif // NO_CONV_TEST


                   }
                   xnewrow += maxm;
                   xlocalrow += maxm;
               }

               swap  = mesh.xlocal;
               mesh.xlocal = mesh.xnew;
               mesh.xnew   = swap;

               /* Convergence testing  */
               #ifndef NO_CONV_TEST
               MPI_Allreduce( &diffnorm, & gdiffnorm, 1, MPI_DOUBLE,
                               MPI_SUM, mesh.ring_comm);

               gdiffnorm = sqrt ( gdiffnorm );

               if (do_print == 1 && rank == 0)
               {
                   printf ("Iteration %2d, diff: %e \n", itcnt, gdiffnorm);
                   fflush (stdout);
               }


               #endif // NO_CONV_TEST
           } while (gdiffnorm > 1.0e-2 && itcnt < max_it);

           t = MPI_Wtime() - t;

       }

      if (rank == 0)
      {
          #ifdef NO_CONV_TEST
                   printf ("%s: %d iterations in %f sec (%f MFlops ), m=%d, n=%d, np=%d",
                   TESTNAME, itcnt, t,
                   itcnt * (maxm - 2.0)*(maxn - 2.0)*(4)*(1.0e-6)/t,
                   maxm, maxn, size  );
          #else
          /* Output if no conv test is choosen */
                   printf ("%s: %d iterations in %f sec (%f MFlops ), diffnorm: %f, m=%d, n=%d, np=%d",
                   TESTNAME, itcnt, t,
                   itcnt * (maxm - 2.0)*(maxn - 2.0)*(4)*(1.0e-6)/t,
                   gdiffnorm,
                   maxm, maxn, size  );

          #endif // NO_CONV_TEST
      }


      MPI_Comm_free (&mesh.ring_comm);
      MPI_Finalize();
      return 0;

  }



