# Makefile for Jacobi Parallel Mesh

#Ref: https://stackoverflow.com/questions/49384301/make-clean-failed-in-windows
ifeq ($(OS),Windows_NT)
	RM = del /Q /F
	CP = copy /Y

	ifdef ComSpec
		SHELL := $(ComSpec)
	endif

	ifdef COMSPEC
		SHELL := $(COMSPEC)
	endif
else
	RM = rm -rf
	CP = cp -f
endif


#Ref: https://makefiletutorial.com/
IDIR="C:\Program Files 2\MPICH2\include"
CC=gcc
CFLAGS= -I$(IDIR)
MPILIB="C:\Program Files 2\MPICH2\lib\mpi.lib"

DEPS= jacobi.h cmdline.h Exchange.h setupmesh.h
OBJ= jacobi.o cmdline.o Exchange.o setupmesh.o

JacobiParallelMesh: $(OBJ)
		$(CC)  -Wall -g  -D NO_CONV_TEST -o JacobiParallelMesh.exe $(OBJ) $(MPILIB)

%.o: %.c $(DEPS)
		$(CC) -Wall -g  -o $@  -c $<  $(CFLAGS)

#Ref: https://www.gnu.org/software/make/manual/html_node/Phony-Targets.html
.PHONY: run

run: JacobiParallelMesh.exe
	mpiexec -np 4 JacobiParallelMesh.exe

.PHONY: clean cleanexe cleanobj

cleanall: cleanexe cleanobj
#	@echo "clean complete"

cleanexe:
	-$(RM) JacobiParallelMesh.exe
#	@echo "clean exe"

cleanobj :
	-$(RM) *.o
	@echo "clean obj"

