#ifndef SETUPMESH_H_INCLUDED
#define SETUPMESH_H_INCLUDED

void Setup_mesh(int maxm, int maxn, Mesh *mesh );
void Init_mesh (Mesh *mesh);

#endif // SETUPMESH_H_INCLUDED
