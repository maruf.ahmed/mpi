#ifndef MESH_H_INCLUDED
#define MESH_H_INCLUDED

   #define  DEFAULT_MAXN  12
   #define  TESTNAME "Jacobi Mesh"

   typedef struct Mesh
   {
    double  *xlocal;
    double  *xnew  ;
    int      maxm  ;
    int      maxn  ;
    int      lrow  ;
    int      up_nbr;
    int      down_nbr  ;
    MPI_Comm ring_comm ;
   } Mesh;



#endif // MESH_H_INCLUDED
