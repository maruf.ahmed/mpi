#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include <string.h>
#include "jacobi.h"
#include "mesh.h"

/*
 * Command line arguments are collected from the process 0
 * and broadcast to other processors.
 *
 * maxm: the number of columns in the global mesh
 * maxn: the number of rows in the global mesh
 *
 * mesh arrays are stored in the row major order
 * Thus, each processor part about ( ( maxm * maxn )/p ) elements.
 * setupmesh.c sets the correct dimensions
 */

 void Get_command_line (int rank, int argc, char **argv,
                        int *maxm, int *maxn, int *do_print, int *maxit)
{
      int args [4];

      if (rank == 0)
      {
           /* Get maxn value from the command line */
           *maxn = DEFAULT_MAXN;
           *maxm = -1;

           for (int i= 1; i < argc; i ++)
           {
               if (argv[i] == NULL)
                   continue;

               if (strcmp (argv[i], "-print") == 0)
                   *do_print = 1;
               else if (strcmp(argv[i], "-n") == 0)
               {
                   *maxn = atoi (argv[i + 1]);
                   i++;
               }
               else if (strcmp(argv[i], "-m") == 0)
               {
                   *maxm = atoi (argv[i + 1]);
                   i++;
               }
               else if (strcmp(argv[i], "-maxit")== 0)
               {
                   *maxit = atoi (argv[i + 1]);
                   i++;
               }

           }
           if (*maxm < 0)
              *maxm = *maxn;
           args [0] = *maxn;
           args [1] = *maxm;
           args [2] = *do_print;
           args [3] = *maxit;
      }
      MPI_Bcast(args, 4, MPI_INT, 0, MPI_COMM_WORLD);
      *maxn     = args [0];
      *maxm     = args [1];
      *do_print = args [2];
      *maxit    = args [3];

}
