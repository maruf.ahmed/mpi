#ifndef CMDLINE_H_INCLUDED
#define CMDLINE_H_INCLUDED

void Get_command_line (int rank, int argc, char **argv,
                        int *maxm, int *maxn, int *do_print, int *maxit);

#endif // CMDLINE_H_INCLUDED
