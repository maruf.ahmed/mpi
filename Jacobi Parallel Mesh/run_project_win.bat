
REM Compile

gcc  -Wall -g  -I"C:\Program Files 2\MPICH2\include"  -c   jacobi.c     -o jacobi.o
gcc  -Wall -g  -I"C:\Program Files 2\MPICH2\include"  -c   cmdline.c    -o cmdline.o
gcc  -Wall -g  -I"C:\Program Files 2\MPICH2\include"  -c   Exchange.c   -o Exchange.o
gcc  -Wall -g  -I"C:\Program Files 2\MPICH2\include"  -c   setupmesh.c  -o setupmesh.
gcc  -Wall -g   -o JacobiParallelMesh.exe  jacobi.o cmdline.o Exchange.o setupmesh.o   "C:\Program Files 2\MPICH2\lib\mpi.lib"


REM Run

mpiexec -n 4 JacobiParallelMesh.exe
